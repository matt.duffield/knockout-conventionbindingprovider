﻿knockout-conventionBindingProvider
================
*knockout-conventionBindingProvider* is a [Knockout.js](http://knockoutjs.com/) plugin that changes the way that Knockout discovers and parses bindings on elements. With this library, Knockout will look for a `data-name` attribute (by default) and uses convention over configuration to create a binding on your view model JavaScript object. It performs several default binding assumptions and tries to keep your markup down to a minimum.  It fully supports falling back onto the standard data-bind mechanism when you need fine grained tuning.  Another feature of the provider is that you can also provide a `data-focus` on any input element and it will set focus to that element.

This library uses the `bindingProvider` extensibility point found in Knockout 2.0+. A description of this functionality and the origin of this implementation can be found in this blog [post](http://www.knockmeout.net/2011/09/ko-13-preview-part-2-custom-binding.html).

What are the benefits of this approach?
---------------------------------------

* The markup can stay clean and simple
* Bindings are created by convention instead of needing any other binding object
* Automatic fallback to knockout's binding provider if the `data-bind` attribute is found
* You can set breakpoints in the bindings to inspect the data being passed through them
* You can do logging in the bindings to understanding how many times they are being called
* You can change/alter the bindings on an element whenever your bindings are triggered
* Bindings go through less parsing (do not need to go from a object literal in a string to code)

Basic Usage
-----------
To get started, reference the `knockout-conventionBindingProvider.js` script after Knockout.js

In your code, prior to calling `ko.applyBindings`, tell Knockout that you want to use a new binding provider.

```js
//options - an object that can contain these properties:
//  attribute - override the attribute used for bindings (defaults to `data-name`)
//  valueUpdate - override the attribute used for when a binding triggers the udpate (defaults to `change`)
var options = { attribute: "data-name", valueUpdate: 'keyup' };
//tell Knockout that we want to use an alternate binding provider
ko.bindingProvider.instance = new ko.conventionBindingProvider(options);

ko.applyBindings(new ViewModel());
```

Sample view model object:

```js
var ViewModel = function () {
    var self = this;

    self.firstName = ko.observable("Matthew");
    self.middleName = ko.observable("Kevin");
    self.lastName = ko.observable("Duffield");
    self.password = ko.observable("");
    self.isActive = ko.observable(false);
    self.canrun = ko.computed(function () {
        return (self.lastName().length > 0 && self.password().length > 0);
    }, self);
    self.run = function () {
        alert("You hit run!");
    };
    self.cantest = ko.computed(function () {
        return self.isActive();
    }, self);
    self.test = function () {
        alert("You hit test!");
    };
    self.canexecute = ko.computed(function () {
        return self.isActive();
    }, self);
    self.execute = function () {
        alert("You hit execute!");
    };
    self.cansubmit = ko.computed(function () {
        return self.isActive();
    }, self);
    self.submit = function () {
        alert("You hit submit!");
    };
};
```

In the example, you can see that we are exposing a standard Knockout view model.  One thing to note is that when we bind up buttons, we also look for a `can...` function on the view model so that we can enable/disable the button automatically.

Then, you would use this view model like the following:

```html
<span data-name="firstName"></span><br/>
<span>First Name:</span>
<input data-name="firstName" /><br />
<span>Middle Name:</span>
<input data-bind="value: middleName" type="text" /><br />
<span>Last Name:</span>
<input data-name="lastName" data-focus="true" /><br />
<span>Password:</span>
<input data-name="password" type="password" /><br />
<span>Is Active:</span>
<input data-name="isActive" type="checkbox" /><br />
<button data-name="run">
    Run
</button>
<span>   </span>
<button data-name="execute">
    Execute
</button>
<span>   </span>
<input data-name="submit" type="submit" value="Submit" />
<span>   </span>
<input data-name="test" type="button" value="Test" /><br />
```

At run-time, you can also access the bindings, by using `ko.bindingProvider.instance.bindings`.  This allows you to add and remove bindings as your application needs them. You can also merge a new set of bindings into the existing bindings using `ko.bindingProvider.instance.registerBindings(newBindings);`.

NOTE:  Notice we can set the focus to an element by adding the `data-focus` attribute.

Dependencies
------------
* Knockout 2.0+

Examples
--------
The examples folder has small samples for normal and AMD usage. Here is the non-AMD sample in jsFiddle: <http://jsfiddle.net/mattduffield/ftbTQ/>.

License
-------
MIT [http://www.opensource.org/licenses/mit-license.php](http://www.opensource.org/licenses/mit-license.php)