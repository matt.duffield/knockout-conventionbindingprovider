﻿
$(document).ready(function () {

    var Country = function (name, population) {
        this.countryName = name;
        this.countryPopulation = population;
    };

    var ViewModel = function () {
        var self = this;

        self.title = ko.observable("sample");
        self.firstName = ko.observable("Matthew");
        self.middleName = ko.observable("Kevin");
        self.lastName = ko.observable("Duffield");
        self.password = ko.observable("");
        self.isActive = ko.observable(false);
        self.flavor = ko.observable("cherry");

        self.availableCountries = ko.observableArray([
                   new Country("UK", 65000000),
                   new Country("USA", 320000000),
                   new Country("Sweden", 29000000)
        ]);
        self.selectedCountry = ko.observable(); // Nothing selected by default

        self.imagePath = ko.observable("/knockout-conventionbindingprovider/content/ilt.png");

        self.canrun = ko.computed(function () {
            return (self.lastName().length > 0 && self.password().length > 0);
        }, self);
        self.run = function () {
            alert("You hit run!");
        };
        self.cantest = ko.computed(function () {
            return self.isActive();
        }, self);
        self.test = function () {
            alert("You hit test!");
        };
        self.canexecute = ko.computed(function () {
            return self.isActive();
        }, self);
        self.execute = function () {
            alert("You hit execute!");
        };
        self.cansubmit = ko.computed(function () {
            return self.isActive();
        }, self);
        self.submit = function () {
            alert("You hit submit!");
        };
        self.canlink = ko.computed(function () {
            return self.isActive();
        }, self);
        self.link = function () {
            alert("You hit link!");
        };
    };


    var options = { attribute: "data-name", valueUpdate: 'keyup' };
        //var options = {};
    //tell Knockout that we want to use an alternate binding provider
    ko.bindingProvider.instance = new ko.conventionBindingProvider(options);

    ko.applyBindings(new ViewModel());
});
