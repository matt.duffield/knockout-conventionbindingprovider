﻿//knockout-conventionBindingProvider v0.0.11 | (c) 2012 Matt Duffield | http://www.opensource.org/licenses/mit-license
!(function (factory) {
    //AMD
    if (typeof define === "function" && define.amd) {
        define(["knockout", "exports"], factory);
        //normal script tag
    } else {
        factory(ko);
    }
}(function(ko, exports, undefined) {
    //a bindingProvider that uses something different than data-bind attributes
    //  options - is an object that can include "attribute" and "valueUpdate" options
    //    "attribute"       - this is the name of the attribute we are trying to check for binding
    //    "valueUpdate"     - this is the binding event that is fired, e.g. change, keyup, keypress, afterkeydown
    //    "anchorTypes"     - represents binding to an A tag
    //    "buttonTypes"     - represents binding to button, submit tags
    //    "checkedTypes"    - represents binding to checkbox and radio tags
    //    "imageTypes"      - represents binding to an IMG tag
    //    "textTypes"       - represents binding to EM, H1, H2, H3, H4, H5, H6, SPAN, STRONG tags
    //    "valueTypes"      - represents binding to password, text, textarea tags
    var conventionBindingProvider = function (options) {
        this.existingProvider = new ko.bindingProvider();

        options = options || {};

        // attribute used for our binding convention.  Default:  "data-name"
        this.attribute = options.attribute || "data-name";

        // defines which browser event KO will use to detect changes. Default: 'change'
        this.valueUpdate = options.valueUpdate || 'change';

        this.anchorTypes = options.buttonTypes || ['A'];
        this.buttonTypes = options.buttonTypes || ['button', 'submit'];
        this.checkedTypes = options.checkedTypes || ['checkbox', 'radio'];
        this.imageTypes = options.imageTypes || ['IMG'];
        this.textTypes = options.textTypes || ['EM', 'H1', 'H2', 'H3', 'H4', 'H5', 'H6', 'SPAN', 'STRONG'];
        this.valueTypes = options.valueTypes || ['password', 'text', 'textarea'];

        //determine if an element has any bindings
        this.nodeHasBindings = function (node) {
            if (node.getAttribute && node.getAttribute("data-bind")) {
                return this.existingProvider.nodeHasBindings(node);
            }
            return node.getAttribute ? node.getAttribute(this.attribute) || node.getAttribute("data-focus") : false;
        };

        //return the bindings given a node and the bindingContext
        this.getBindings = function getBindings(node, bindingContext) {
            var result = {};

            // If a binding is already setup, use it.
            if (node.getAttribute && node.getAttribute("data-bind")) {
                result = this.existingProvider.getBindings(node, bindingContext);
            } else {
                var item = node.getAttribute(this.attribute);
                if (item) {
                    //var desc = node.id + " (" + node.nodeName + " - " + node.type + ")";
                    var bindingAccessor = this.mapBindings(node, item, bindingContext);
                    if (bindingAccessor) {
                        var binding = typeof bindingAccessor == "function" ?
                        bindingAccessor.call(bindingContext.$data) : bindingAccessor;
                        ko.utils.extend(result, binding);
                    }
                }
            }

            // Apply focus() on the node containing the data-focus attribute.
            item = node.getAttribute("data-focus");
            if (item != null) {
                node.focus();
            }

            return result;
        };

        this.mapBindings = function (node, bindingName, bindingContext) {
            var obj = {};
            //valueUpdate: 'change'
            obj["valueUpdate"] = this.valueUpdate;

            if (this.isBinding(node.nodeName, this.anchorTypes)) {
                return function () {
                    //click: run
                    obj["click"] = bindingContext.$data[bindingName];
                    return obj;
                };
            }
            else if (this.isBinding(node.type, this.buttonTypes)) {
                return function () {
                    var bindingCanName = "can" + bindingName;
                    //click: run
                    //enable: canrun
                    obj["click"] = bindingContext.$data[bindingName];
                    if (bindingContext.$data[bindingCanName] != null) {
                        obj["enable"] = bindingContext.$data[bindingCanName];
                    }
                    return obj;
                };
            }
            else if (this.isBinding(node.type, this.checkedTypes)) {
                return function () {
                    //checked: isActive
                    obj["checked"] = bindingContext.$data[bindingName];
                    return obj;
                };
            }
            else if (this.isBinding(node.type, this.valueTypes)) {
                return function () {
                    //value: firstName
                    obj["value"] = bindingContext.$data[bindingName];
                    return obj;
                };
            }
            else if (this.isBinding(node.nodeName, this.textTypes)) {
                return function () {
                    //text: title
                    obj["text"] = bindingContext.$data[bindingName];
                    return obj;
                };
            }
            else if (this.isBinding(node.nodeName, this.imageTypes)) {
                return function () {
                    //attr: { src: imagePath };
                    obj["attr"] = {};
                    obj["attr"]["src"] = bindingContext.$data[bindingName];
                    return obj;
                };
            }

            return null;
        };

        this.isBinding = function (value, types) {
            if ($.inArray(value, types) > -1) {
                return true;
            }
            else {
                return false;
            }
        };

    };

    if (!exports) {
        ko.conventionBindingProvider = conventionBindingProvider;
    }

    return conventionBindingProvider;
}));